using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pila_estatica
{
    class Program
    {
        static void Main(string[] args)
        {
            PilaNodo p = new PilaNodo();
            p.insertar("HOla");
            p.insertar("Mundo");
            p.quitar();
            p.insertar("Espacio");
            p.insertar("______________");
            p.insertar("***************");
            p.Imprimir();

            
            Console.ReadKey();

          


        }
    }

    class Nodo
    {
        public string info;
        public Nodo sig;
    }


    //declaramos una estructura de dato Nodo
    class PilaNodo {        
                
        private Nodo raiz;
        int prueba;
        //se declara un primer objeto raiz de tipo nodo
        public PilaNodo()
        {
            raiz = null;            
        }
        //se asigna la raiz a null
        public void insertar(string x)
        {
            Nodo nuevo= new Nodo();
            nuevo.info = x;
            if (raiz == null)
            {
                nuevo.sig = null;
                raiz = nuevo;
            }
            else
            {
                nuevo.sig = raiz;
                raiz = nuevo;
            }
        }

        public string quitar()
        {
                string informacion = raiz.info;
                raiz = raiz.sig;
                return informacion;

        }

        public void Imprimir()
        {
            Nodo reco = raiz;
            Console.WriteLine("Lista de informacion");
            while (reco != null)
            {
                Console.WriteLine(reco.info);
                reco = reco.sig;
            }
            Console.WriteLine();
        }
    }
}
